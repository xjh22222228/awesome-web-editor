



<div align="center">
  <div>
    <img src="https://raw.githubusercontent.com/xjh22222228/awesome-web-editor/master/media/logo.png" />
  </div>
  <p>开源WEB编辑器汇总</p>
  <div>
    <a href="https://github.com/xjh22222228/awesome-web-editor/">GitHub仓库</a>&nbsp;&nbsp;&nbsp;
    <a href="https://github.com/xjh22222228/awesome-web-editor/issues">贡献</a>&nbsp;&nbsp;&nbsp;
    <a href="https://github.com/xjh22222228/">My GitHub</a>
  </div>
</div>
</br></br></br>


## 更新说明 / Update description
每个自然月更新一次开源WEB编辑器Star。

不定时更新优秀WEB编辑器。



---


## 富文本编辑器 / Rich text editor
- vue.js
  - [vue-quill-editor](https://github.com/surmon-china/vue-quill-editor) ★ 2581 - 基于 Quill、适用于 Vue 的富文本编辑器，支持服务端渲染和单页应用。
  - [iview-editor](https://github.com/iview/iview-editor) ★ 68 - iView Editor 是基于 iView 的 markdown 编辑器，支持上传图片（可集成七牛等服务），支持全屏实时编辑预览。
- react
  - [draft-js](https://github.com/facebook/draft-js) ★ 13462 - A React framework for building text editors.
  - [slate](https://github.com/ianstormtaylor/slate) ★ 8056 - A completely customizable framework for building rich text editors.
- [wangEditor](https://github.com/wangfupeng1988/wangEditor) ★ 5267 - 国产轻量级web富文本框。
- [ueditor](https://github.com/fex-team/ueditor) ★ 3778 - rich text 百度富文本编辑器。
- [quill](https://github.com/quilljs/quill) ★ 19015 - Quill is a modern WYSIWYG editor built for compatibility and extensibility.
- [kindeditor](https://github.com/kindsoft/kindeditor) ★ 1261 - WYSIWYG HTML editor.
- [simditor](https://github.com/mycolorway/simditor) ★ 3902 - An Easy and Fast WYSIWYG Editor.
- [tinymce](https://github.com/tinymce/tinymce) ★ 5522 - The world's most popular JavaScript library for rich text editing. Available for React, Vue and Angular.
- [jquery-notebook](https://github.com/raphaelcruzeiro/jquery-notebook) ★ 1702 - A modern, simple and elegant WYSIWYG rich text editor.
- [mercury](https://github.com/jejacks0n/mercury) ★ 2593 - Mercury Editor: The Rails HTML5 WYSIWYG editor.
- [bootstrap-wysiwyg](https://github.com/mindmup/bootstrap-wysiwyg/) ★ 5561 - Tiny bootstrap-compatible WISWYG rich text editor
- [ckeditor5](https://github.com/ckeditor/ckeditor5) ★ 1226 - Development environment for CKEditor 5 – the best browser-based rich text editor.
- [summernote](https://github.com/summernote/summernote) ★ 7541 - Super simple WYSIWYG editor, Summernote is built on jQuery.







---


## markdown编辑器 / Markdown editor
- vue.js
  - [mavonEditor](https://github.com/hinesboy/mavonEditor) ★ 1377 -   A markdown editor based on Vue that supports a variety of personalized features.
- [simplemde-markdown-editor](https://github.com/sparksuite/simplemde-markdown-editor) ★ 5328 -  A simple, beautiful, and embeddable JavaScript Markdown editor. Delightful editing for beginners and experts alike. Features built-in autosaving and spell checking.
- [editor.md](https://github.com/pandao/editor.md) ★ 5846 - 可嵌入的 Markdown 在线编辑器（组件），基于 CodeMirror、jQuery 和 Marked 构建。
- [editor](https://github.com/lepture/editor) ★ 2567 - A markdown editor.
- [dillinger](https://github.com/joemccann/dillinger) ★ 6024 - The last Markdown editor, ever.
- [tui.editor](https://github.com/nhnent/tui.editor) ★ 6928 - Markdown WYSIWYG Editor. GFM Standard + Chart & UML Extensible.





---




## markdown解析器 / Markdown parser
- [marked](https://github.com/markedjs/marked) ★ 16920 - A markdown parser and compiler. Built for speed.
- [showdown](https://github.com/showdownjs/showdown) ★ 7187 - A bidirectional MD to HTML to MD converter written in Javascript.
- [markdown-it](https://github.com/markdown-it/markdown-it) ★ 5718 - Markdown parser, done right. 100% CommonMark support, extensions, syntax plugins & high speed.
- [markdown-js](https://github.com/evilstreak/markdown-js) ★ 6820 - A Markdown parser for javascript.




---



## 浏览器编辑器 / Browser editor
- markdown
  - [stackedit](https://github.com/benweet/stackedit) ★ 12305 - In-browser Markdown editor.
  - [markdown-editor](https://github.com/jbt/markdown-editor) ★ 2259 - Live (Github-flavored) Markdown Editor.
- [CodeMirror](https://github.com/codemirror/CodeMirror) ★ 15195 - In-browser code editor.



## 其他 / Other
- [github-markdown-css](https://github.com/sindresorhus/github-markdown-css) ★ 2973 - The minimal amount of CSS to replicate the GitHub Markdown style.
- [markdown基本语法](https://github.com/younghz/Markdown) ★ 1050 - markdown基本语法教程



---


## 贡献 / Contribution
Thank you for your [contribution](https://github.com/xjh22222228/awesome-web-editor/issues), men.

<a href="https://github.com/1c7/">
  <img src="https://avatars1.githubusercontent.com/u/1804755?s=460&v=4" width="30px" height="30px" />
</a>






## License
[MIT](https://opensource.org/licenses/MIT)

只要注明原作者许可声明，您可以自由地复制、分享、和修改。

Copyright (c) 2018-present, [xiejiahe](https://github.com/xjh22222228)